#!/usr/bin/env bash
#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

curl "https://demo.freedombox.org/plinth" > /dev/null
status=$?

# Alert on IRC channel if demo server is down
# To be used with a cron job
if [[ $status -ne 0 ]]; then
    python3 $DIR/send_irc_message.py --server 'irc.oftc.net' --port 6697 --channel '#freedombox-ci' --nick 'monitorbot' --message "njoseph: Demo server is down!!!"
fi
